#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import re
import time
import getpass
import copy
from operator import *


import pprint
import pickle
import os

JOB_DATA_CACHE_FILE = os.path.join(os.path.dirname(__file__), '../../others', 'jobData.pkl')


class PickleTaskInfo(object):
    """docstring for PickleTaskInfo"""

    def __init__(self, pickleFile):
        super(PickleTaskInfo, self).__init__()

        self.pickleFile = pickleFile

    def save_data_to_pickle(self, data):
        _file = self.pickleFile

        output = open(_file, 'wb')

        # Pickle dictionary using protocol 0.
        pickle.dump(data, output)

        # Pickle the list using the highest protocol available.
        # pickle.dump(selfref_list, output, -1)

        output.close()

    def load_data_from_pickle(self):

        _file = self.pickleFile

        pkl_file = open(_file, 'rb')

        data1 = pickle.load(pkl_file)

        pkl_file.close()

        return data1


class TaskInfoObj(object):
    _verbose = False

    def runtime(self, *data):
        spaceNum = 30
        runtimeStr = ''
        strList = []

        for i, d in enumerate(data):
            _str = '{' + str(i) + ':' + str(spaceNum) + 's}:   '
            strList.append(str(d))

            if i < (len(data) - 1):
                runtimeStr += _str
            else:
                runtimeStr += '{' + str(len(data) - 1) + ':s}\n'

        runtimeStr = runtimeStr.format(*strList)
        # runtimeStr = '{0:15s}:     {1:s}'.format( str(title), str(data) )

        if self._verbose:
            print runtimeStr

        return runtimeStr

    def __init__(self):
        pass
        self.pickleFile = JOB_DATA_CACHE_FILE

    # get job object list
    def get_job_obj_list(self):
        # newJobList      = []
        obj = PickleTaskInfo(self.pickleFile)
        newJobList = obj.load_data_from_pickle()

        return newJobList

    # kill job list (not implimented yet)
    def kill_job_id_list(self, job_id_list):
        jobData = ['']
        jobData.extend(job_id_list)

        _result = True
        return _result

    # remove job list (not implimented yet)
    def remove_job_id_list(self, job_id_list):
        jobData = ['']
        jobData.extend(job_id_list)

        _result = True
        return _result


# ----------------------------------- test -------------------------------------

def include_keys(orig, keys):
    filtered = dict(zip(keys, [orig[k] for k in keys]))
    return filtered


import random
import json


def createFakeData(startID, taskNum):
    # startID = 30590
    # taskNum = 5000

    dataList = ['todotally', 'todo', 'status', 'name', 'user', 'id']
    groups = []
    nameList = ['Emily', 'James', 'Chloe', 'Jack', 'Megan', 'Alex',
                'Charlotte', 'Ben', 'Emma', 'Daniel', 'Lauren', 'Tom']
    showList = ['Batman', 'Test', 'WonderWoman', 'Snoopy']
    statusList = ['complete', 'complete', 'complete', 'complete',
                  'complete', 'complete', 'complete', 'complete', 'complete', 'failed', 'killed', 'blocked', 'pending', 'running']
    sw = ['maya_rendering', 'maya_animation', 'maya_rigging', 'nuke', 'python']

    taskPattern = '''{'status': '%(status)s', 'name': '[%(show)s] %(sw)s_%(user)s_v00%(ver)s', 'todotally': {'complete': %(complete)s, 'badlogin': 0, 'unknown': 0, 'waiting': 0, 'failed': 0, 'running': 0, 'suspended': 0, 'killed': 0, 'pending': 0, 'blocked': 0}, 'user': '%(user)s', 'todo': %(todo)s, 'group': '3d', 'id': %(id)d}'''
    taskPattern = taskPattern.replace("'", "\"")

    def genDict(id):
        numLow = 10
        numHigh = 190
        todo = random.randint(numLow, numHigh)
        complete = random.randint(numLow, todo)
        ver = random.randint(1, 7)

        dic = {'user': (random.choice(nameList)),
               'show': (random.choice(showList)),
               'status': (random.choice(statusList)),
               'sw': (random.choice(sw)),
               'complete': complete,
               'ver': ver,
               'id': id,
               'todo': todo}
        if dic['status'] == 'complete':
            dic['complete'] = todo
        if dic['sw'] == 'python':
            dic['group'] = 'cmd'
        if dic['sw'] == 'nuke':
            dic['group'] = '2d'
        return dic

    return [(json.loads(taskPattern % genDict(i))) for i in range(startID, startID + taskNum)]


def saveFakeData():
    startID = 30590
    taskNum = 1000

    fakeData = createFakeData(startID, taskNum)
    obj = PickleTaskInfo(JOB_DATA_CACHE_FILE)
    obj.save_data_to_pickle(fakeData)
    print fakeData[0]
    # for d in fakeData:
    #     print d, type(d)


def execFunc():
    taskObj = TaskInfoObj()
    _list = taskObj.get_job_obj_list()
    print '============ job list ============'
    print len(_list)
    # dataList = ['todotally', 'todo', 'status', 'name', 'user', 'id']
    # d = include_keys(_list[0], dataList)
    # print d

if __name__ == "__main__":
    pass
    saveFakeData()
