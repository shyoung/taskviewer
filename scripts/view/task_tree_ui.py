#!/usr/bin/env python
# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui

import os
import sys
#import codecs
import operator

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

COL_TID = 0
COL_STTS = 1
COL_PROG = 2
COL_NAME = 3
COL_USER = 4
COL_CPU = 5
COL_CLST = 6
COL_GRUP = 7

status_dict = dict()

# incomplete
# status_dict['pending'] = QtGui.QColor(255, 255, 255)
# status_dict['blocked'] = QtGui.QColor(255, 255, 200)
# status_dict['running'] = QtGui.QColor(216, 255, 216)
# status_dict['failed'] = QtGui.QColor(255, 216, 216)

# status_dict['killed'] = QtGui.QColor(230, 220, 200)
# status_dict['dying'] = QtGui.QColor(245, 235, 215)

# status_dict['complete'] = QtGui.QColor(216, 229, 254)
# ------------------------------------
status_dict['pending'] = QtGui.QColor(72, 72, 72)
status_dict['blocked'] = QtGui.QColor(130, 81, 0)
status_dict['running'] = QtGui.QColor(40, 90, 50)
status_dict['failed'] = QtGui.QColor(145, 0, 0)

status_dict['killed'] = QtGui.QColor(99, 81, 44)
status_dict['dying'] = status_dict['killed']

status_dict['complete'] = QtGui.QColor(10, 0, 80)

"""
Qt.white    3    White (#ffffff)
Qt.black    2    Black (#000000)
Qt.red    7    Red (#ff0000)
Qt.darkRed    13    Dark red (#800000)
Qt.green    8    Green (#00ff00)
Qt.darkGreen    14    Dark green (#008000)
Qt.blue    9    Blue (#0000ff)
Qt.darkBlue    15    Dark blue (#000080)
Qt.cyan    10    Cyan (#00ffff)
Qt.darkCyan    16    Dark cyan (#008080)
Qt.magenta    11    Magenta (#ff00ff)
Qt.darkMagenta    17    Dark magenta (#800080)
Qt.yellow    12    Yellow (#ffff00)
Qt.darkYellow    18    Dark yellow (#808000)
Qt.gray    5    Gray (#a0a0a4)
Qt.darkGray    4    Dark gray (#808080)
Qt.lightGray    6    Light gray (#c0c0c0)
Qt.transparent    19    a transparent black value (i.e., QColor(0, 0, 0, 0))
Qt.color0    0    0 pixel value (for bitmaps)
Qt.color1    1    1 pixel value (for bitmaps)
"""


class QTaskTreeWidgetItem(QtGui.QTreeWidgetItem):

    defaultColor = QtCore.Qt.black

    def __init__(self, taskid, status, name, user, percent, running, cpu, cluster, group, parent=None):
        super(QTaskTreeWidgetItem, self).__init__(parent)

        self.__taskid__ = taskid
        self.__status__ = status
        self.__name__ = name
        self.__user__ = user
        self.__percent__ = percent
        self.__running__ = running
        self.__cpu__ = cpu
        self.__cluster__ = cluster
        self.__group__ = group

        self.setText(COL_TID, u'%d' % taskid)
        self.setText(COL_STTS, u'%s' % status)
        self.setText(COL_NAME, u'%s' % name)
        self.setText(COL_USER, u'%s' % user)
        self.setText(COL_PROG, u'%d%s' % (int(percent * 100), '%'))
        # self.setText(COL_CPU, u'%d/%d' % (running, cpu))
        # self.setText(COL_CLST, u'%s' % cluster)
        self.setText(COL_GRUP, u'%s' % group)

        self.__bg_paint__()

        #self.setIcon(0, QtGui.QIcon())

    @property
    def taskid(self):
        return self.__taskid__

    @property
    def status(self):
        return self.__status__

    @status.setter
    def status(self, new):
        if self.__status__ != new:
            self.__status__ = new

            self.__bg_paint__()
            self.setText(COL_STTS, u'%s' % new)

    def __bg_paint__(self):

        color = self.defaultColor

        if status_dict.has_key(self.__status__):
            color = status_dict.get(self.__status__)

            self.setBackgroundColor(COL_TID,  color)
            self.setBackgroundColor(COL_STTS, color)
            self.setBackgroundColor(COL_PROG, color)
            self.setBackgroundColor(COL_NAME, color)
            self.setBackgroundColor(COL_USER, color)
            self.setBackgroundColor(COL_CPU, color)
            self.setBackgroundColor(COL_CLST, color)
            self.setBackgroundColor(COL_GRUP, color)

        # self.drawline()

    @property
    def name(self):
        return self.__name__

    @name.setter
    def name(self, new):
        if self.__name__ != new:
            self.__name__ = new
            self.setText(COL_NAME, u'%s' % new)

    @property
    def user(self):
        return self.__user__

    @user.setter
    def user(self, new):
        if self.__user__ != new:
            self.__user__ = new
            self.setText(COL_USER, u'%s' % new)

    @property
    def percent(self):
        return self.__percent__

    @percent.setter
    def percent(self, new):
        if self.__percent__ != new:
            self.__percent__ = new
            self.setText(COL_PROG, u'%s' % new)

    def __repr__(self):
        return "<Task:%06d[%s] state:%s(%d) user:%s >" % (self.taskid, self.name, self.status, self.percent, self.user)

    def update(self, status, name, user, percent, running):
        self.status = status
        self.name = name
        self.user = user
        self.percent = percent
        self.__running__ = running
        # self.__cpu__ = cpu


class QTaskTreeWidget(QtGui.QTreeWidget):

    trigger = QtCore.pyqtSignal(QtGui.QTreeWidgetItem, int)

    lineColor = QtGui.QColor(227, 227, 227)

    def __init__(self, _taskContainer, parent=None):
        super(QTaskTreeWidget, self).__init__(parent)
        self.setup_ui()

        self.tempText = QtCore.QString("")

        self.taskContainer = _taskContainer

        self.setAlternatingRowColors(True)

        super(QTaskTreeWidget, self).setSelectionMode(QtGui.QAbstractItemView.ContiguousSelection)

        # QtGui.QAbstractItemView.ContiguousSelection
        # QtGui.QAbstractItemView.MultiSelection

        super(QTaskTreeWidget, self).setSortingEnabled(True)

        """   
    #overloaded
    def drawBranches (self, painter, rect, index ) :
        #QTreeView.drawBranches (self, QPainter painter, QRect rect, QModelIndex index)
        super( QTaskTreeWidget, self).drawBranches( painter, rect, index )
 
        s = index.sibling( index.row(), 0 );
        #if s.isValid():
            #rect = self.visualRect( s )
        py = rect.y()
        ph = rect.height()
        pw = rect.width()
        painter.setPen( QtGui.QColor( 82, 82, 82 ) )
        painter.drawLine( 0, py + ph, pw + 10, py + ph )  
    """

    def paintEvent(self, event):
        super(QTaskTreeWidget, self).paintEvent(event)
        #painter = QtGui.QPainter(self)
        # See: http://stackoverflow.com/questions/12226930/overriding-qpaintevents-in-pyqt
        painter = QtGui.QPainter(self.viewport())
        painter.setPen(self.lineColor)
        _it = QtGui.QTreeWidgetItemIterator(self)
        try:
            _item = _it.value()
            while _item != None:
                for col in range(_item.columnCount()):

                    index = self.indexFromItem(_item, col)

                    rect = self.visualRect(index)
                    painter.drawRect(rect)
                    #px = rect.x()
                    #py = rect.y()
                    #ph = rect.height()
                    #pw = rect.width()
                    #painter.drawLine( px, py + ph, pw, py + ph )

                _item = _it.value()
                _it += 1
        finally:
            123
            del _it
            # painter.end()

    def setup_ui(self):

        titleList = QtCore.QStringList()
        titleList.append('ID')
        titleList.append('status')
        titleList.append('percent')
        titleList.append('Name')
        titleList.append('User')
        titleList.append('Instances')
        titleList.append('Cluster')
        titleList.append('Group')

        self.setHeaderLabels(titleList)

        self.setColumnWidth(COL_TID,  80)
        self.setColumnWidth(COL_STTS,  70)
        self.setColumnWidth(COL_PROG,  50)
        self.setColumnWidth(COL_NAME, 300)
        self.setColumnWidth(COL_USER, 100)
        self.setColumnWidth(COL_CPU, 60)
        self.setColumnWidth(COL_CLST, 80)
        self.setColumnWidth(COL_GRUP, 100)

        self.connect_ui()

    def connect_ui(self):

        self.itemChanged.connect(self.event_itemChanged)
        self.itemPressed.connect(self.event_itemPressed)
        self.itemClicked.connect(self.event_itemClicked)

    def __getItemByID__(self, taskID, _operator):
        _it = QtGui.QTreeWidgetItemIterator(self)
        _item = _it.value()
        while _item != None:
            if _operator(_item.taskid, taskID):
                break
            _it += 1
            _item = _it.value()
        del _it

        return _item

    def remove_data(self, _taskid):

        self.release_floating_widget()

        _item = self.__getItemByID__(_taskid, operator.eq)

        if _item:
            idx = self.indexOfTopLevelItem(_item)
            retItem = self.takeTopLevelItem(idx)
            del retItem

        if self.taskContainer.has_key(_taskid):
            self.taskContainer.pop(_taskid)

    def remove_data_list(self, taskid_list):

        for _taskid in taskid_list:
            self.remove_data(_taskid)

    def get_data_selected(self):

        # list-of-QTreeWidgetItem
        _list = self.selectedItems()
        for item in _list:
            yield item.taskid

    def clear(self):

        super(QTaskTreeWidget, self).clear()

        self.release_floating_widget()
        self.taskContainer.clear()

    def release_floating_widget(self):
        pass

    def update_data(self, data, filterSet):
        self.release_floating_widget()

        running, cpu, cluster = ['', '', '']
        try:
            taskID, status, name, user, percent, group = data
            # taskID, status, name, user, percent, running, cpu, cluster, group = data
            pass
        except:
            print "Unexpected error:", sys.exc_info()[0]

        self.taskContainer[taskID] = data

        #==============================================================================

        _item = self.__getItemByID__(taskID, operator.eq)
        if not _item:
            # QtGui.QTreeWidgetItem()
            _item = QTaskTreeWidgetItem(taskid=taskID,
                                        status=status,
                                        name=name,
                                        user=user,
                                        percent=percent,
                                        running=running,
                                        cpu=cpu,
                                        cluster=cluster,
                                        group=group,
                                        parent=self)
            self.addTopLevelItem(_item)

        else:
            _item.update(status, name, user, percent, running)

        if _item.status in filterSet:
            _item.setHidden(False)
        else:
            _item.setHidden(True)

    def filter_view(self, filterSet):

        _it = QtGui.QTreeWidgetItemIterator(self)
        _item = _it.value()
        while _item != None:

            if _item.status in filterSet:
                _item.setHidden(False)
            else:
                _item.setHidden(True)

            _it += 1
            _item = _it.value()

        del _it

    #================================================

    def event_itemClicked(self, item, column):

        return
        print 'event_itemClicked %s' % item

        if self.indexOfTopLevelItem(item) < 3:
            pass

        if column >= 0:
            # TODO:
            pass

    def event_itemPressed(self, item, column):

        return
        print 'event_itemPressed %s' % item

        if item.flags() & QtCore.Qt.ItemIsEditable == QtCore.Qt.ItemIsEditable:
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)

    def event_itemChanged(self, item, column):
        return
        # print 'event_itemChanged'
        pass
        # if  item.flags() & QtCore.Qt.ItemIsEditable  == QtCore.Qt.ItemIsEditable:
        # TODO:
        """
        if column == COL_CHK: # start edit!
            #print item.text(self.currentColumn())
            self.tempText = item.text(self.currentColumn())
        else:
            if (column == COL_PRI) :
                retTurple = item.text(column).toFloat() #.toFloat()
                #print retTurple
                if retTurple[1] == False :
                    item.setText (column, self.tempText)
                elif retTurple[0] < 0 or retTurple[0] > 2000:
                    item.setText (column, self.tempText) 
        """

    def keyPressEvent(self, KeyEvent):

        if KeyEvent.matches(QtGui.QKeySequence.Delete):
            print 'delete'
            pass
        else:
            super(QTaskTreeWidget, self).keyPressEvent(KeyEvent)


def test():
    pass
    import sys

    application = QtGui.QApplication(sys.argv)
    myUI01 = MyMainWindow(controller=None)
    myUI01.connect_control(_controller)
    myUI01.show()

    # myUI01.time_event_test()

    sys.exit(application.exec_())


if __name__ == '__main__':
    pass
