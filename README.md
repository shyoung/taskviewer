# Task Viewer

Task Viewer is a python GUI for monitoring the progress of all jobs. 
It offers wide-range of color and options to distinguish different job status. 
The current version is loading a dumped cache file for demo.

![image](https://bytebucket.org/shyoung/taskviewer/raw/c3ca5b1786a9c5661ad4ca56438024a4eecff5f1/task.viewer.screenshot.jpg)

##### News

| Date     | Version   | Event
|:---------|:----------|:----------
| Sep 2017 | [0.1][] | First release of TaskViewer

### Usage

TaskViewer is known to run on Windows and Linux under Python 2.7 with PyQt4.
Use "main.sh" or "main.bat" to launch tool.

