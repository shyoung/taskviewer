#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
from PyQt4 import QtCore, QtGui

import tab_ui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


def functor(name):
    def foo():
        print name
    return foo

#==========================================================


class tabSet(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))

        # UI layout

        verticalLayout = QtGui.QVBoxLayout(Dialog)
        verticalLayout.setObjectName(_fromUtf8("verticalLayout"))

        self.tabWidget = QtGui.QTabWidget(Dialog)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))

        self.taskTab = tab_ui.taskWidget(Dialog)
        self.taskTab.setObjectName(_fromUtf8("taskTab"))

        self.tabWidget.addTab(self.taskTab, _fromUtf8(""))
        #=========================================

        verticalLayout.addWidget(self.tabWidget)

        #=========================================
        Dialog.layout().addWidget(self.tabWidget)
        # Dialog.setLayout(verticalLayout)

        self.load_tabs()
        self.tabWidget.setCurrentIndex(0)

        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def load_tabs(self):
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.taskTab),    QtGui.QApplication.translate(
            "Dialog", "Jobs", None, QtGui.QApplication.UnicodeUTF8))

    def update_data(self, data):
        self.taskTab.update_data(data)

    def connect_control(self, controller):
        self.taskTab.connect_control(controller)

    def mainWidget(self):
        return self.taskTab

#==========================================================
"""
class MyDialog(QtGui.QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self) 
        self.__pushItem__()        
                
    def __pushItem__(self):
        pass
"""
#==========================================================


class MyWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.tabSet = tabSet()
        self.tabSet.setupUi(self)

    def update_data(self, data):
        self.tabSet.update_data(data)

    def connect_control(self, controller):
        self.tabSet.connect_control(controller)


class MyMainWindow(QtGui.QMainWindow):

    trigger = QtCore.pyqtSignal()

    idx = 10
    #now = None

    def __init__(self, controller=None, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setupUi()
        self.connect_control(controller)

    def setupUi(self):

        self.setWindowTitle(QtGui.QApplication.translate(
            "Dialog", "Task Viewer v0.1", None, QtGui.QApplication.UnicodeUTF8))

        # team_menu = QtGui.QMenuBar(parent=self)
        # team_menu.addAction(u'About', functor('About tool'))
        # self.setMenuBar(team_menu)

        self.mainWid = MyWindow(parent=self)

        self.setCentralWidget(self.mainWid)

        self.setStatusBar(QtGui.QStatusBar(parent=self))

        self.print_status('ready')

        #==================================

    def connect_control(self, controller):
        self.mainWid.connect_control(controller)

        #self.mainWid.tabSet.mainWidget().treeCtrl.button_refresh.pressed.connect(self.print_pending )
        #self.mainWid.tabSet.mainWidget().treeCtrl.button_team.pressed.connect(   self.print_pending )

    def print_status(self, msg):
        self.statusBar().showMessage(msg)

    def print_pending(self):
        self.print_status('pending...')

    def print_ready(self):
        self.print_status('ready')


def create_ui(_controller):

    import sys
    application = QtGui.QApplication(sys.argv)
    myUI01 = MyMainWindow(controller=None)
    myUI01.connect_control(_controller)
    myUI01.show()
    sheet = os.path.normpath(os.path.join(__file__, '../../../others/QTDark.stylesheet'))
    print sheet

    sheetData = ''
    with open(sheet) as file:
        sheetData = file.read()

    application.setStyleSheet(sheetData)
    sys.exit(application.exec_())
