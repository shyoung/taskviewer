#!/usr/bin/env python
# -*- coding: utf-8 -*-


from PyQt4 import QtCore, QtGui

import os
#import codecs

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from task_tree_ui import QTaskTreeWidget

#=====================================================


def functor(name, button):
    def foo():
        return
        if isinstance(button, QtGui.QPushButton):
            button.setText(name)
    return foo

task_dict = dict()


class QTeamMamu(QtGui.QMenu):

    triggerMenuTriggered = QtCore.pyqtSignal(QtGui.QAction)

    def __init__(self, button, parent=None):
        QtGui.QWidget.__init__(self, parent=parent)

        self.parent_button = button
        self.action_container = dict()
        self.current_action = None

        self.setup_ui()

    def setup_ui(self):
        pass

    def update_cluster_list(self, all_cluster_list, user_cluster_list, cookie_cluster):

        current_team = None
        is_cooike_applied = False

        for e, _team in enumerate(all_cluster_list):
            action = self.addAction(_team, functor(_team, self.parent_button))
            self.action_container[action] = _team
            if not is_cooike_applied:
                if _team in user_cluster_list or e == 0:
                    self.current_action = action
                    self.parent_button.setText(_team)
                    current_team = _team

                if _team == str(cookie_cluster):
                    self.current_action = action
                    self.parent_button.setText(_team)
                    current_team = _team
                    is_cooike_applied = True

        return current_team

    def update_team_list(self, all_team_list, user_team):

        current_team = None
        is_cooike_applied = False

        for e, _team in enumerate(all_team_list):
            action = self.addAction(_team, functor(_team, self.parent_button))
            self.action_container[action] = _team
            if not is_cooike_applied:
                if _team == user_team or e == 0:
                    self.current_action = action
                    self.parent_button.setText(_team)
                    current_team = _team

        return current_team

    def get_team_name_by_action(self, action):
        self.current_action = action
        return self.action_container.get(action)

    def get_current_team_name(self):
        return self.action_container.get(self.current_action)

#======================================


class QSearchToolButton(QtGui.QToolButton):

    def __init__(self, icon_path, text, parent=None):
        super(QSearchToolButton, self).__init__(parent)

        self._icon_path = icon_path
        self._text = text

        self.setup_ui()

    def setup_ui(self):

        _icon = QtGui.QIcon(self._icon_path)

        self.setText(self._text)
        self.setFixedSize(70, 48)
        self.setIcon(_icon)
        self.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.setIconSize(QtCore.QSize(24, 24))


class QFilterToolButton(QSearchToolButton):

    def __init__(self, icon_path, text, filterSet, parent=None):

        super(QFilterToolButton, self).__init__(icon_path, text, parent)

        self._filterSet = filterSet
        #self._downState = False

        self.setup_ui()

    def setup_ui(self):
        super(QFilterToolButton, self).setup_ui()

        #super(QFilterToolButton, self).setAutoRaise(False)
        super(QFilterToolButton, self).setCheckable(True)
        super(QFilterToolButton, self).setChecked(True)

    @property
    def filterSet(self):
        return self._filterSet

#QtGui.QToolBar('123',parent = mainWindow)


class JobToolBar(QtGui.QToolBar):

    trigger = QtCore.pyqtSignal()
    triggerBool = QtCore.pyqtSignal(bool)

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, 'JobToolBar', parent=parent)

        self.filter_button_list = []
        self.setup_ui()

    def setup_ui(self):

        # self.setFloatable(False)
        self.setMovable(False)

        self.setFixedHeight(52)
        # self.setFixedWidth(1000)
        self.setAutoFillBackground(True)

        # self.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)

        # print os.path.dirname(os.path.realpath(__file__))

        _icon_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..', 'icons')
        # _icon_dir = os.path.join( os.getcwd(), '..', '..', 'icons' )
        # print '_icon_dir:   ', _icon_dir

        # _icon_dir = 'icons'

        #===============================================================

        refresh_icon_path = os.path.join(_icon_dir, 'view_refresh.png')

        self.button_refresh = QSearchToolButton(refresh_icon_path, 'Refresh', self)
        self.addWidget(self.button_refresh)

        #===============================================================

        # filters
        self.addWidget(QtGui.QLabel(u'   ', self))
        self.addSeparator()
        self.addWidget(QtGui.QLabel(u'   ', self))

        incomplete_icon_path = os.path.join(_icon_dir, 'queue_pending.png')

        self.button_incomplete = QFilterToolButton(
            incomplete_icon_path, 'Incomplete', set(['pending', 'blocked']), self)
        self.filter_button_list.append(self.button_incomplete)

        running_icon_path = os.path.join(_icon_dir, 'queue_running.png')

        self.button_running = QFilterToolButton(
            running_icon_path, 'Running', set(['running']), self)
        self.filter_button_list.append(self.button_running)

        failed_icon_path = os.path.join(_icon_dir, 'queue_failed.png')

        self.button_failed = QFilterToolButton(failed_icon_path, 'Failed', set(['failed']), self)
        self.filter_button_list.append(self.button_failed)

        killed_icon_path = os.path.join(_icon_dir, 'queue_killed.png')

        self.button_killed = QFilterToolButton(
            killed_icon_path, 'Killed', set(['killed', 'dying']), self)
        self.filter_button_list.append(self.button_killed)

        complete_icon_path = os.path.join(_icon_dir, 'queue_done.png')

        self.button_complete = QFilterToolButton(
            complete_icon_path, 'Complete', set(['complete']), self)
        self.filter_button_list.append(self.button_complete)

        #===============================================================

        for button in self.filter_button_list:
            self.addWidget(button)
            #button.downState = True
            # button.setDown(True)

        #===============================================================

        #kill & remove
        self.addWidget(QtGui.QLabel(u'   ', self))
        self.addSeparator()
        self.addWidget(QtGui.QLabel(u'   ', self))

        kill_icon_path = os.path.join(_icon_dir, 'stop.PNG')

        self.button_kill = QSearchToolButton(kill_icon_path, 'Kill Job', self)
        self.addWidget(self.button_kill)

        #===============================================================

        remove_icon_path = os.path.join(_icon_dir, 'remove_D1.PNG')

        self.button_remove = QSearchToolButton(remove_icon_path, 'Remove Job', self)
        self.addWidget(self.button_remove)

        #===============================================================

        team_icon_path = os.path.join(_icon_dir, 'team_B.PNG')

        self.button_team = QSearchToolButton(team_icon_path, 'team', self)
        self.addWidget(self.button_team)
        self.button_team.setFixedSize(90, 48)

        team_menu = QTeamMamu(button=self.button_team, parent=self)

        self.button_team.setMenu(team_menu)
        self.button_team.setPopupMode(QtGui.QToolButton.InstantPopup)
        #{ DelayedPopup, MenuButtonPopup, InstantPopup }

        # self.addWidget(self.button_team)

        self.connect_ui()

    def connect_ui(self):

        # self.button_incomplete.pressed.connect(self.event_pressed)

        # self.button_refresh.pressed.connect(self.event_pressed)
        # self.button_refresh.released.connect(self.event_released)
        # self.button_refresh.toggled.connect(self.event_toggled)

        # self.button_team.pressed.connect(self.event_pressed)

        #self.button_kill.connect(   self.event_itemPressed)
        #self.button_remove.connect( self.event_itemClicked)
        #self.button_team.connect(   self.event_itemClicked)
        pass

    def connect_filter_button(self, func):

        for button in self.filter_button_list:
            # button.clicked.connect(func(button))
            # button.pressed.connect(func(button))
            button.released.connect(func(button))

    def filterSet(self):

        _set = set()

        for button in self.filter_button_list:
            if button.isChecked():
                _set |= button.filterSet

        return _set

    def update_cluster_list(self, cluster_list, user_cluster, cookie_cluster):

        team_menu = self.button_team.menu()
        return team_menu.update_cluster_list(cluster_list, user_cluster, cookie_cluster)

    def update_team_list(self, team_list, user_team):

        team_menu = self.button_team.menu()
        return team_menu.update_team_list(team_list, user_team)

    def get_team_name_by_action(self, action):
        team_menu = self.button_team.menu()
        return team_menu.get_team_name_by_action(action)

    def get_current_team_name(self):
        team_menu = self.button_team.menu()
        return team_menu.get_current_team_name()

    def set_team_name(self, name):
        self.button_team.setText(name)

    def event_clicked(self):
        print 'button clicked'

    def event_pressed(self):
        print 'button pressed'

    def event_released(self):
        print 'button released'

    def event_toggled(self):
        print 'button toggled'

delay_update = True


class taskWidget(QtGui.QWidget):

    trigger = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi()
        self.controller = None

        self.dirty = False
        # self.dirty = True

        self.set_to_send = set()

        self.temp_filter = set()

        if delay_update:
            self.query_ret = []

    def setupUi(self):

        self.treeCtrl = JobToolBar(self)
        self.parent().parent().addToolBar(QtCore.Qt.TopToolBarArea, self.treeCtrl)

        self.treeCtrl.setObjectName(_fromUtf8("treeCtrl"))

        self.treeWidget = QTaskTreeWidget(task_dict, self)
        self.treeWidget.setObjectName(_fromUtf8("treeWidget"))

        self.treeQLabel = QtGui.QTextEdit(u'', self)
        self.treeQLabel.setFrameStyle(QtGui.QFrame.StyledPanel)
        self.treeQLabel.setObjectName(_fromUtf8("LabelImg"))
        self.treeQLabel.setText(_fromUtf8("Log Detail"))
        self.treeQLabel.setFixedHeight(150)

        # self.treeQLabel.setMinimumHeight(150)

        verticalLayout = QtGui.QVBoxLayout(self)
        verticalLayout.setObjectName(_fromUtf8("verticalLayoutTEXT"))

        # verticalLayout.addWidget(self.treeCtrl)
        verticalLayout.addWidget(self.treeWidget)
        verticalLayout.addWidget(self.treeQLabel)

        self.timer = QtCore.QTimer(parent=self)
        self.timer.setInterval(3)
        self.timer.start()

        self.setBaseSize(600, 600)

        self.connect_ui()

    def connect_ui(self):

        # function button

        self.treeCtrl.button_refresh.clicked.connect(self.event_refresh_selected)

        self.treeCtrl.button_kill.clicked.connect(self.event_kill_selected)
        self.treeCtrl.button_remove.clicked.connect(self.event_remove_selected)

        self.treeCtrl.button_team.menu().triggered.connect(self.event_menu_triggered)

        # filter buttons

        self.treeCtrl.connect_filter_button(self.event_filter_clicked)

        self.timer.timeout.connect(self.timout_event)

    def connect_control(self, controller):
        self.controller = controller
        # self.treeCtrl.connect_control(controller)

        if controller:

            controller.get_logger(self.log)

            self.log('User: %s' % controller.get_user())
            self.log('User is lead: %s' % controller.user_is_lead())

            #user_cluster = self.treeCtrl.update_cluster_list( controller.request_cluster_info(), controller.request_user_clusters(), controller.get_memory_cluster() )
            user_team = self.treeCtrl.update_team_list(
                controller.request_team_list(), controller.request_user_team())

            self.update_data_list(user_team)

            self.visibility_update(user_team)

    def log(self, msg):
        self.treeQLabel.append(msg)
        self.treeQLabel.moveCursor(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)

    def update_data(self, data):
        self.treeWidget.update_data(data, self.treeCtrl.filterSet())

    def update_data_list(self, team):

        self.treeWidget.clear()

        if self.controller:
            # members = self.controller.request_table()
            # self.controller.request_task_by_cluster(cluster)
            members = self.controller.get_team_members(team)
            self.controller.request_task_by_userlist(members)
            self.temp_filter = self.treeCtrl.filterSet()
            if self.dirty == True:
                self.set_to_send.clear()
                self.query_ret = []
            else:
                self.dirty = True
        return

    def __data_feedback_check(self, set_to_send):

        if self.controller:
            self.controller.request_task_by_idlist(set_to_send)
            self.temp_filter = self.treeCtrl.filterSet()
            if self.dirty == True:
                self.set_to_send.clear()
                self.set_to_send |= set_to_send
                self.query_ret = []
            else:
                self.set_to_send |= set_to_send
                self.dirty = True

    def event_kill_selected(self):

        set_to_send = set(self.treeWidget.get_data_selected())

        if self.controller:
            self.controller.kill_tasks(set_to_send)

            self.__data_feedback_check(set_to_send)

    def event_remove_selected(self, func):

        set_to_send = set(self.treeWidget.get_data_selected())
        if self.controller:
            self.controller.remove_tasks(set_to_send)

            self.__data_feedback_check(set_to_send)

    def event_refresh_selected(self):

        team_name = self.treeCtrl.get_current_team_name()
        print 'team_name', team_name
        self.update_data_list(team_name)

    def event_menu_triggered(self, action):

        team_name = self.get_team_changed(action)
        self.update_data_list(team_name)

    def get_team_changed(self, action):

        team_name = self.treeCtrl.get_team_name_by_action(action)

        self.treeCtrl.set_team_name(team_name)
        self.visibility_update(team_name)

        return team_name

    def visibility_update(self, team_name):

        authorization = False
        if team_name != None:
            user_team = self.controller.request_user_team()
            if user_team == 'visitor':
                authorization = True
            if self.controller.user_is_lead():
                # for user_cluster in self.controller.request_task_by_userlist(
                # self.controller.get_team_members() ):
                if user_team in team_name:
                    authorization = True
                elif user_team.lower() == self.controller.ADMIN.lower():
                    authorization = True

        self.treeCtrl.button_kill.setEnabled(authorization)
        self.treeCtrl.button_remove.setEnabled(authorization)

    def try_finish_refresh(self):
        if len(self.query_ret) == 0:
            self.log('Removed: %s ' % self.set_to_send)
            self.treeWidget.remove_data_list(self.set_to_send)
            self.set_to_send.clear()
            self.dirty = False
            self.treeWidget.filter_view(self.treeCtrl.filterSet())

    def timout_event(self):
        # print 'timout_event =========='
        # print 'delay_update', delay_update
        if delay_update:
            # print 'self.dirty', self.dirty
            # print 'self.query_ret', len(self.query_ret)
            idx = 0
            while len(self.query_ret) > 0 and idx < 30:
                idx += 1
                data = self.query_ret.pop()
                did = data[0]
                # if did % 20 == 0:
                #    self.log( 'data id %d' % did ) #, verbose = True
                # print 'data', data
                self.treeWidget.update_data(data, self.temp_filter)
                self.set_to_send.discard(did)
                self.try_finish_refresh()
            if idx > 0:
                return

            if self.dirty:
                if self.controller:
                    self.query_ret = [x for x in self.controller.request_table()]
                    # print 'self.query_ret 11', self.query_ret
                    self.try_finish_refresh()
                    # print 'self.query_ret 22', self.query_ret

        else:
            if self.dirty:
                if self.controller:
                    for data in self.controller.request_table():
                        print data
                        self.treeWidget.update_data(data, self.temp_filter)
                        self.set_to_send.discard(data[0])
                    self.treeWidget.remove_data_list(self.set_to_send)
                self.set_to_send.clear()
                self.dirty = False

    def event_filter_clicked(self, button):

        def func():

            #button.downState = not button.downState

            #self.log('filterSet: %s' % button.parent().filterSet())

            self.treeWidget.filter_view(button.parent().filterSet())

            return

        return func
