#!/usr/bin/env python
# -*- coding: utf-8 -*-


import csv
# reference:
# http://stackoverflow.com/questions/904041/reading-a-utf8-csv-file-with-python

encoding = "utf-8"


class UnicodeCsvReader(object):

    def __init__(self, f, encoding=encoding, **kwargs):
        self.csv_reader = csv.reader(f, **kwargs)
        self.encoding = encoding

    def __iter__(self):
        return self

    def next(self):
        # read and split the csv row into fields
        row = self.csv_reader.next()
        # now decode
        return [unicode(cell, self.encoding) for cell in row]

    @property
    def line_num(self):
        return self.csv_reader.line_num


class UnicodeDictReader(csv.DictReader):

    def __init__(self, f, encoding=encoding, fieldnames=None, **kwds):
        csv.DictReader.__init__(self, f, fieldnames=fieldnames, **kwds)
        self.reader = UnicodeCsvReader(f, encoding=encoding, **kwds)
