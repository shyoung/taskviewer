#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __init__ import USE_CLUSTER

import os
import logging
import logging.handlers
# import getpass

# GROUP_LIST = ['3d', '2d', 'cmd']


def doRollover(cls):
    """
    Do a rollover, as described in __init__().
    """
    import shutil
    cls.stream.close()
    if cls.backupCount > 0:
        for i in range(cls.backupCount - 1, 0, -1):
            sfn = "%s.%d" % (cls.baseFilename, i)
            dfn = "%s.%d" % (cls.baseFilename, i + 1)
            if os.path.exists(sfn):
                # print "%s -> %s" % (sfn, dfn)
                if os.path.exists(dfn):
                    os.remove(dfn)
                os.rename(sfn, dfn)
        dfn = cls.baseFilename + ".1"
        if os.path.exists(dfn):
            os.remove(dfn)
        # print "%s -> %s" % (cls.baseFilename, dfn)
        shutil.copy2(cls.baseFilename, dfn)
        #os.rename( cls.baseFilename, dfn )
    cls.mode = 'w'
    cls.stream = cls._open()


def init_logger(log_name, _level=logging.INFO):

    _logger = None

    if type(log_name) == str:
        _logger = logging.getLogger(log_name)
    else:
        _logger = logging.getLogger()

    #============================================

    if len(_logger.handlers) > 0:
        for hdlr in _logger.handlers:
            _logger.removeHandler(hdlr)
            hdlr.close()

    #============================================

    # Setup Handler
    __log_file__ = _logger.name + '.log'

    if True:

        _fileHandler = logging.handlers.RotatingFileHandler(
            filename=__log_file__, maxBytes=1024 * 1024 * 1, backupCount=10, encoding='utf-8')
        _fileHandler.setLevel(_level)

        delattr(logging.handlers.RotatingFileHandler, 'doRollover')
        setattr(logging.handlers.RotatingFileHandler, 'doRollover', doRollover)

        formatter = logging.Formatter(
            '[%(asctime)s]  [%(levelname)s]  [%(threadName)s(%(thread)d)] - %(message)s ')
        _fileHandler.setFormatter(formatter)

        _logger.addHandler(_fileHandler)

    if True:
        _console = logging.StreamHandler()
        _console.setLevel(_level)
        _console_formatter = logging.Formatter('%(message)s')
        _console.setFormatter(_console_formatter)

        _logger.addHandler(_console)

    _logger.setLevel(_level)

    _logger.log(level=_level, msg='=====================LOG STARTED=====================',)

    return _logger


class base(object):

    def __init__(self, verbose):

        self.verbose = verbose
        self.ui_hnd = None
        self.logger = init_logger('Task.Viewer.' + __name__, logging.INFO)
        # self._team_list = GROUP_LIST
        # self.user = getpass.getuser()

    def __create_core__(self):
        raise

    def get_user(self):
        self.log('CALL get_user(void) -> <un-impl>')
        return self.user

    def user_is_lead(self):
        self.log('CALL user_is_lead(void) -> <un-impl>')
        return True

    #==============================================

    if USE_CLUSTER:

        def request_user_clusters(self):
            self.log('CALL request_user_clusters(void) -> <un-impl>')
            return ['/zz']

        def request_cluster_info(self):
            self.log('CALL request_cluster_info(void) -> <un-impl>')
            return []

        #==============================================

        def get_memory_cluster(self):
            self.log('CALL get_memory_cluster(void) -> <un-impl>')

        def set_memory_cluster(self, cluster):
            self.log('CALL set_memory_cluster(%s) -> <un-impl>' % cluster)

    #==============================================

    def request_user_team(self):
        self.log('CALL request_user_team(void) -> <un-impl>')

    def request_team_list(self):
        self.log('CALL request_team_info(void) -> %s' % self._team_list)
        return self._team_list

    def get_team_members(self, team):
        self.log('CALL get_team_members(void) -> <un-impl>')

    #==============================================

    def request_task_by_cluster(self, cluster):
        self.log('CALL request_task_by_cluster(%s) -> (iter)' % cluster)

    def request_task_by_idlist(self, idlist):
        self.log('CALL request_task_by_idlist(%s) -> (iter)' % idlist)

    def request_task_by_userlist(self, userlist):
        self.log('CALL request_task_by_userlist(%s) -> (iter)' % userlist)

    def request_table(self):
        self.log('CALL request_table(void) -> <un-impl>')

    #==============================================

    def kill_tasks(self, taskid_list):
        self.log('CALL<un-impl> kill_tasks(%s)' % taskid_list)
        return True

    def remove_tasks(self, taskid_list):
        self.log('CALL<un-impl> remove_tasks(%s)' % taskid_list)
        return True

    #==============================================

    def get_logger(self, ui_hnd):
        self.ui_hnd = ui_hnd
        return True

    def log(self, msg, verbose=None):

        #loggerlevel = logging.INFO

        # if self.logger.isEnabledFor( loggerlevel ):
        #    self.logger.log(level = loggerlevel, msg = msg )

        print msg

        if verbose == None:
            if not self.verbose:
                return
        elif verbose == True:
            pass
        else:
            return

        try:
            self.ui_hnd(msg)
        except:
            pass
