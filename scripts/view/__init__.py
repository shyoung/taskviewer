#!/usr/bin/env python
# -*- coding: utf-8 -*-

import window_ui


def create_ui(controller):

    return window_ui.create_ui(controller)
