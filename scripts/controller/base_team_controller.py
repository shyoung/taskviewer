#!/usr/bin/env python
# -*- coding: utf-8 -*-

from base import base

from __init__ import USE_CLUSTER

import iniMemorizer

import getpass
import csv
import csv_unicode
import tempfile
import os
import codecs

temp_dir = tempfile.gettempdir()
COOKIE_PATH = os.path.join(temp_dir, 'task.viewer/cookie')

CLUSTER = u'CLUSTER'


def get_list_from_csv(path):

    print path

    with open(path, 'r') as f:
        for row in csv.DictReader(f):
            # for row in csv_unicode.UnicodeDictReader(f):
            yield row


def ensure_create_dir(_path):
    if not os.path.exists(_path):
        os.makedirs(_path)
        return True
    return False


def dict_list_insert(_dict, key, value):

    if not key:
        return

    if len(key) < 2:
        return

    if len(key.strip()) == 0:
        return

    value = value.lower()

    _list = _dict.get(key)
    if _list:
        _list.append(value)
    else:
        _list = []
        _list.append(value)
        _dict[key] = _list


class Impl(base):

    ADMIN = u'admin'

    def __init__(self, conf_path, verbose=False):

        super(Impl, self).__init__(verbose=verbose)

        self.config = iniMemorizer.get(conf_path)
        self.user = getpass.getuser()
        csv_file = os.path.normpath(os.path.join(
            __file__, '../../..', self.config.get("authorization")))
        self.my_info_dict = {}

        account_struct = list(get_list_from_csv(csv_file))

        for user_info in account_struct:
            user = user_info.get(u"Account").lower().strip()
            if user == self.user.lower():
                self.my_info_dict = user_info
                break

        _my_team = self.my_info_dict.get(u"Team")

        if _my_team == None:
            self.my_team = "visitor"
        else:
            self.my_team = _my_team.lower().strip()

        self.log('my team: %s' % self.my_team)

        self.team_member_dict = {}

        for user_info in account_struct:
            _user = user_info.get(u"Account")

            _team = user_info.get(u"Team")
            dict_list_insert(self.team_member_dict, _team, _user)

        self._team_list = self.team_member_dict.keys()
        self._team_list.sort()

        if self.my_team.lower() != self.ADMIN:
            try:
                self._team_list.remove(self.ADMIN)
            except:
                pass

        ensure_create_dir(os.path.dirname(COOKIE_PATH))

    def get_user(self):
        self.log('CALL get_user(void) -> %s' % self.user)
        return self.user

    def user_is_lead(self):

        if self.my_info_dict.has_key(u"Leader"):
            ret = self.my_info_dict.get(u"Leader").lower() == 'TRUE'.lower()
            self.log('CALL user_is_lead(void) -> %s' % ret)
            return ret
        return False

    #==============================================

    def request_user_team(self):
        self.log('CALL request_user_team(void) -> %s' % self.my_team)
        return self.my_team

    def request_team_list(self):
        self.log('CALL request_team_info(void) -> %s' % self._team_list)
        return self._team_list

    def get_team_members(self, team):
        self.log('CALL get_team_members(void) -> %s' % self.team_member_dict.get(team))
        return self.team_member_dict.get(team)

    #==============================================

    if USE_CLUSTER:

        def request_user_clusters(self):

            string = self.my_info_dict.get(u"Cluster")
            if not string:
                return []
            ret = [x.strip().lower() for x in string.split(',')]

            if self.ADMIN in ret:
                ret = self.request_cluster_info()
            self.log('CALL request_user_clusters(void) -> %s' % ret)
            return ret

        # def request_cluster_info( self ):

        def get_memory_cluster(self):

            info_dict = iniMemorizer.get(COOKIE_PATH)

            return info_dict.get(CLUSTER)

        def set_memory_cluster(self, cluster):

            _dict = {CLUSTER: cluster}
            iniMemorizer.set(COOKIE_PATH, _dict)
