#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __init__ import USE_CLUSTER

import base_team_controller


class Impl(base_team_controller.Impl):

    def __init__(self, conf_path, verbose=True):

        super(Impl, self).__init__(conf_path=conf_path, verbose=verbose)

        self.taskObj = self.__create_core__()

        # print self.taskObj

        self.killed_temp = set()

        self.removed_temp = set()

    #==============================================

    def __create_core__(self):
        try:
            from scripts.core.get_task_info import TaskInfoObj
            self.log('CALL __create_core__(void) -> %s' % TaskInfoObj)
            return TaskInfoObj()
        except Exception as e:
            self.log('CALL __create_core__(void) -> failed: %s' % e)
            return None

    #==============================================

    if USE_CLUSTER:
        def request_cluster_info(self):
            self.log('CALL request_team_info(void) %s' % self.taskObj.getClusterInfoDict())
            return self.taskObj.getClusterInfoDict()

    #==============================================

    def request_table(self):
        _list = self.taskObj.get_job_obj_list()

        conut = 0
        for task in _list:
            _id = task['id']
            if _id in self.removed_temp:
                continue
            elif _id in self.killed_temp:
                conut += 1
                _ratio = (float(task['todotally']['complete']) / task['todo'])
                yield _id, 'killed', task['name'], task['user'], _ratio, task['group']
            else:
                conut += 1
                _ratio = (float(task['todotally']['complete']) / task['todo'])

                yield _id, task['status'], task['name'], task['user'], _ratio, task['group']

        self.log('size of feedback -> %d' % conut, verbose=True)

    #==============================================

    def kill_tasks(self, taskid_set):
        self.log('CALL kill_tasks(%s)' % taskid_set)
        self.killed_temp |= taskid_set

        return True

    def remove_tasks(self, taskid_set):
        self.log('CALL remove_tasks(%s)' % taskid_set)
        self.removed_temp |= taskid_set

        return True

if __name__ == "__main__":

    import os

    currentPath = os.path.dirname(os.path.realpath(__file__))

    conf_path = r'%s/config.ini' % currentPath
    print conf_path

    controller = Impl(conf_path)

    if USE_CLUSTER:
        print 'request_cluster_info'
        print controller.request_cluster_info()

    print 'request_table'
    for task in controller.request_table():
        print task
