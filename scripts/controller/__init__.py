#!/usr/bin/env python
# -*- coding: utf-8 -*-

USE_CLUSTER = False


def create(conf_path):

    import fake_controller
    return fake_controller.Impl(conf_path)

    # import task_controller
    # return task_controller.Impl(conf_path)
