#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import os
from scripts import controller
from scripts import view


import atexit


if __name__ == "__main__":

    currentPath = os.path.dirname(os.path.realpath(__file__))

    conf_path = r'%s/config.ini' % currentPath
    print conf_path
    controller = controller.create(conf_path)
    view.create_ui(controller)
