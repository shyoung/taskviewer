#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

#==============================================================


def get(_filename):

    try:
        jObj = ''
        with open(_filename, "r+") as rawfile:
            jObj = rawfile.read()
        # rawfile.close()
        _struct = json.loads(jObj)
    except:
        _struct = {}

    return _struct


def set(_filename, newtypeDict):

    jObj = json.dumps(newtypeDict)

    with open(_filename, "w+") as _file:
        _file.write(jObj)
    #_file = open( _filename, "w+")
    #_file.write(jObj)
    #_file.close()

    return newtypeDict


def update(_filename, newtypeDicts):

    _typeDict = get(_filename)

    for newtypeDict in newtypeDicts:
        _typeDict.update(newtypeDict)

    jObj = json.dumps(_typeDict)

    with open(_filename, "w+") as _file:
        _file.write(jObj)
    #_file.close()

    return _typeDict
